#include <stdio.h>
#include <unistd.h>
#define SUCCESS 0
#define FAIL 1

// placeholder macro until undelete system command is available
#define undelete(filename) system("echo UnDeleting file: " #filename)

// Postive test case: undelete all files in a directory
static int test1()
{
    int ret;

    system("touch test1");
    system("touch test2");
    system("rm test1");
    system("rm test2");
    ret = undelete();
    if (ret == SUCCESS)
        return SUCCESS;
    else
        return FAIL;
}


// Postive test case: undelete a given file
static int test2()
{
    int ret;

    system("touch test1");
    system("touch test2");
    system("rm test1");
    system("rm test2");
    ret = undelete(test2);
    if (ret == SUCCESS)
        return SUCCESS;
    else
        return FAIL;
}

int undelete_main()
{
  int rc = SUCCESS;
  if (test1() == FAIL)
  {
    printf("Test1 undelete failed\n");
    rc = FAIL;
  }

  if (test2() == FAIL)
  {
    printf("Test2 undelete failed\n");
    rc = FAIL;
  }

  if (rc == SUCCESS)
  {
	printf("All 2 Tests of undelete Passed\n");
  }

  return rc;
}
