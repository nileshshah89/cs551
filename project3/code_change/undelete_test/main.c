#include <stdio.h>
#include <unistd.h>
#include "undelete_test.h"
int main()
{
    int ret;
    ret = undelete_main();
    //ret |= further_main();
    if (ret != SUCCESS)
    {
        printf("Test(s) failed\n");
        return FAIL;
    }

    printf("-------------------");
    printf("All tests passed");
    printf("-------------------\n");
    return SUCCESS;
}
