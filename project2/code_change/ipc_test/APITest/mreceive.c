#include <stdio.h>
#include <unistd.h>

#define PASS 0
#define FAIL -1

// Negative test case: Test for negative grp id
static int test1()
{
  int ret;
  char c[20] = "hi";
  ret = mreceive(-1, c);
  if (ret == -1)
    return PASS;
  else
    return FAIL; 
}

// Negative test case: Checks mreceive when called for uncreated group id 
static int test2()
{
  int ret;
  char c[20] = "hi";
  ret = mreceive(1, c);
  if (ret == FAIL)
    return PASS;
  else
    return FAIL; 
}

// Negative test case: No of group limit is 32, pass more than it
static int test3()
{
  int ret;
  char c[20] = "hi";
  ret = mreceive (33, c);
  if (ret == -1)
    return PASS;
  else
    return FAIL; 
}

// Negative test case: send NULL in message
static int test4()
{
  int ret;
  ret = mreceive (1, NULL);
  if (ret == -1)
    return PASS;
  else
    return FAIL; 
}

int mreceive_main()
{
  if (test1() == FAIL)
  {
    printf("Test1 mreceive failed\n");
    return FAIL;
  }

  if (test2() == FAIL)
  {
    printf("Test2 mreceive failed\n");
    return FAIL;
  }
  
  if (test3() == FAIL)
  {
    printf("Test3 mreceive failed\n");
    return FAIL;
  }

  if (test4() == FAIL)
  {
    printf("Test4 mreceive failed\n");
    return FAIL;
  }

  printf("All 4 Test of mreceive Passed\n");
  return 0;
}
