#include <stdio.h>
#include <unistd.h>

// Negative test case: Test negative group id
static int test1()
{
    int ret;
    ret = closeGroup(-1, 0);
    if (ret == FAIL)
        return SUCCESS;
    else
        return FAIL;
}


// Negative test case: Test group id exceeding max 32
static int test2()
{
    int ret;
    ret = closeGroup(33, 0);
    if (ret == FAIL)
        return SUCCESS;
    else
        return FAIL;
}

// Negative test case: Test close group for 0
static int test3()
{
    int ret;
    ret = closeGroup(0, 0);
    if (ret == FAIL)
    {
        return SUCCESS;
    }
    else
    {
        return FAIL;
    }
}

// Positive test case: Test closeGroup does delete the group entries
static int test4()
{
    int ret;
    ret = openGroup(31, 0);
    if (ret != FAIL)
    {
        ret = closeGroup(31, 0);
        if (ret == FAIL)
        {
            return FAIL;
        }
        else
        {
            return SUCCESS;
        }
    }
    else return FAIL;
}

int closeGroup_main()
{
  if (test1() == FAIL)
  {
    printf("Test1 closeGroup failed\n");
    return FAIL;
  }

  if (test2() == FAIL)
  {
    printf("Test2 closeGroup failed\n");
    return FAIL;
  }
  
  if (test3() == FAIL)
  {
    printf("Test3 closeGroup failed\n");
    return FAIL;
  }

  if (test4() == FAIL)
  {
    printf("Test4 closeGroup failed\n");
    return FAIL;
  }

  printf("All 4 Test of closeGroup Passed\n");
  return 0;
}
