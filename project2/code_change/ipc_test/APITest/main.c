#include <stdio.h>
#include <unistd.h>
#include "api_test.h"
int main()
{
    int ret;
    ret = msend_main();
    ret |= mreceive_main();
    ret |= openGroup_main();
    ret |= closeGroup_main();
    ret |= recoverGroup_main();
    if (ret != SUCCESS)
    {
        printf("Test failed\n");
        return FAIL;
    }

    printf("-------------------");
    printf("All test passed");
    printf("-------------------\n");
    return SUCCESS;
}
