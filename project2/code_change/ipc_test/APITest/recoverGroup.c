#include <stdio.h>
#include <unistd.h>
#define SUCCESS 0
#define FAIL -1

// Negative test case: Test negative group id
static int test1()
{
    int ret;
    ret = recoverGroup(-1);
    if (ret == FAIL)
        return SUCCESS;
    else
        return FAIL;
}


// Negative test case: Test group id exceeding max 32
static int test2()
{
    int ret;
    ret = recoverGroup(33);
    if (ret == FAIL)
        return SUCCESS;
    else
        return FAIL;
}

// Negative test case: Test if invalid groud id it passed
static int test3()
{
    int ret;
    ret = recoverGroup(0);
    if (ret == FAIL)
    {
        return SUCCESS;
    }
    else
    {
        return FAIL;
    }
}

// Positive test case: recover from gid 1
static int test4()
{
    int ret;
    ret = recoverGroup(1);
    if (ret == SUCCESS)
    {
        return SUCCESS;
    }
    else
    {
        return FAIL;
    }
}

int recoverGroup_main()
{
  if (test1() == FAIL)
  {
    printf("Test1 recoverGroup failed\n");
    return FAIL;
  }

  if (test2() == FAIL)
  {
    printf("Test2 recoverGroup failed\n");
    return FAIL;
  }
  
  if (test3() == FAIL)
  {
    printf("Test3 recoverGroup failed\n");
    return FAIL;
  }

  if (test4() == FAIL)
  {
    printf("Test4 recoverGroup failed\n");
    return FAIL;
  }

  printf("All 4 Test of recoverGroup Passed\n");
  return 0;
}
