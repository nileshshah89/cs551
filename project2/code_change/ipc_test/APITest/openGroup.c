#include <stdio.h>
#include <unistd.h>

// Negative test case: Test negative group id
static int test1()
{
    int ret;
    ret = openGroup(-1, 0);
    if (ret == FAIL)
        return SUCCESS;
    else
        return FAIL;
}


// Negative test case: Test group id exceeding max 32
static int test2()
{
    int ret;
    ret = openGroup(33, 0);
    if (ret == FAIL)
        return SUCCESS;
    else
        return FAIL;
}

// Negative test case: Test creation of group where api returns group id
static int test3()
{
    int ret;
    ret = openGroup(0, 0);
    if (ret == FAIL)
    {
        return FAIL;
    }
    else
    {
        if (ret != 0) return SUCCESS;
        else return FAIL;
    }
}

// Positive test case: Create the group
static int test4()
{
    int ret;
    ret = openGroup(16, 0);
    if (ret == FAIL)
    {
        return FAIL;
    }
    else
    {
        if (ret == 16)
        {
            closeGroup(16, 0);
            return SUCCESS;
        }
        else return FAIL;
    }
}

int openGroup_main()
{
  if (test1() == FAIL)
  {
    printf("Test1 openGroup failed\n");
    return FAIL;
  }

  if (test2() == FAIL)
  {
    printf("Test2 openGroup failed\n");
    return FAIL;
  }
  
  if (test3() == FAIL)
  {
    printf("Test3 openGroup failed\n");
    return FAIL;
  }

  if (test4() == FAIL)
  {
    printf("Test4 openGroup failed\n");
    return FAIL;
  }

  printf("All 4 Test of openGroup Passed\n");
  return 0;
}
