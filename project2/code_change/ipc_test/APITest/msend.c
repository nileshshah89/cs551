#include <stdio.h>
#include <unistd.h>

#define PASS 0
#define FAIL -1

// Negative test case: Test for negative grp id
static int test1()
{
  int ret;
  char c[20] = "hi";
  ret = msend(-1, c);
  if (ret == FAIL)
    return PASS;
  else
    return FAIL; 
}

// Negative test case: Checks msend when called for uncreated group id
static int test2()
{
  int ret;
  char c[20] = "hi";
  ret = msend(1, c);
  if (ret == FAIL)
    return PASS;
  else
    return FAIL; 
}

// Negative test case: No of group limit is 32, pass more than it
static int test3()
{
  int ret;
  char c[20] = "hi";
  ret = msend (33, c);
  if (ret == FAIL)
    return PASS;
  else
    return FAIL; 
}

// Negative test case: send NULL in message
static int test4()
{
  int ret;
  ret = msend (33, NULL);
  if (ret == FAIL)
    return PASS;
  else
    return FAIL; 
}

// Negative test case: If message exceeds the limit
static int test5()
{
  int ret;
  char c[100] = "adbseradadferqwefrqpwoeifrqwpeoijrqpwoeiqewqj";
  ret = msend (1, c);
  if (ret == FAIL)
    return PASS;
  else
    return FAIL; 
}

int msend_main()
{
  if (test1() == FAIL)
  {
    printf("Test1 msend failed\n");
    return FAIL;
  }

  if (test2() == FAIL)
  {
    printf("Test2 msend failed\n");
    return FAIL;
  }
  
  if (test3() == FAIL)
  {
    printf("Test3 msend failed\n");
    return FAIL;
  }

  if (test4() == FAIL)
  {
    printf("Test4 msend failed\n");
    return FAIL;
  }

  if (test5() == FAIL)
  {
    printf("Test5 msend failed\n");
    return FAIL;
  }

  printf("All 5 Test of msend Passed\n");
  return 0;
}
