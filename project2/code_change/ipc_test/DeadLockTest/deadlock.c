#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

/* This test case creates 5 childs which waits on message
 * from group id 31. Parent recoverGroup(31).
 * Once all child gets unblocked and writes verbose log,parent checks for it.
 * If valid messages are found in file, test passes.
 */

#define CHILDBODY {                                                             \
    int ret;                                                                    \
    char message[15];                                                           \
    FILE *fd;                                                                   \
    fd = fopen("temp.dat", "a+");                                               \
    if (fd == NULL)                                                             \
    {                                                                           \
        printf("Error: fopen failed\n");                                        \
        return FAIL;                                                            \
    }                                                                           \
    ret = openGroup(31, 0);                                                     \
    ret = mreceive(ret, message);                                               \
    fprintf(fd, "Recovered from mreceive in pid = %d\n", getpid());             \
    printf("Recovered from mreceive in pid = %d\n", getpid());                  \
    fclose(fd);                                                                 \
    return SUCCESS;                                                             \
}                                                      

int main()
{
    int ret;
    int gid;
    ret = closeGroup(31, 0);
    if (ret != FAIL)
    {
        pid_t pid;
        
        // Create child 1
        pid = fork();
        if (pid == 0) CHILDBODY
        
        // Create child 2
        pid = fork();
        if (pid == 0) CHILDBODY
        
        // Create child 3
        pid = fork();
        if (pid == 0) CHILDBODY
        
        // Create child 4
        pid = fork();
        if (pid == 0) CHILDBODY
        
        // Create child 5
        pid = fork();
        if (pid == 0) CHILDBODY

        else
        {
            char c;
            FILE *fd;
            int count_line = 0;
            pid_t pid;

            // Let childs run first
            sleep(5);

            ret = recoverGroup(31);
            
            // Wait till all child checks for recover flag
            sleep(5);

            // All child will write message in temp.dat file
            fd = fopen("temp.dat", "r");
            if (fd == NULL)
            {
                printf("Error in fopen in parent\n");
                return FAIL;
            }

            // Count the number of lines in temp.dat
            c = getc(fd);
            while (c != EOF)
            {
                if (c == '\n') count_line++;
                c = getc(fd);
            }

            fclose(fd);

            // Number of lines in temp.dat must be equal to number of child
            if (count_line == 5)
            {
                printf ("--------------Test passed--------------\n");    
            }
            else
            {
                printf ("--------------Test failed------------------\n");
            }

            //Delete temp.dat file
            pid = fork();
            if (pid == 0)
            {
                execlp("rm", "rm", "temp.dat", NULL);
            }

            return SUCCESS;
        }
    }
    else
    {
        printf("Error: openGroup failed\n");
    }
}
