#include "pm.h"
#include <minix/callnr.h>
#include <signal.h>
#include <sys/svrctl.h>
#include <sys/reboot.h>
#include <sys/resource.h>
#include <sys/utsname.h>
#include <minix/com.h>
#include <minix/config.h>
#include <minix/sysinfo.h>
#include <minix/type.h>
#include <minix/ds.h>
#include <machine/archtypes.h>
#include <lib.h>
#include <assert.h>
#include <string.h>
#include "mproc.h"

#define PM_SUCCESS 0

// 2D Table where group information is stored
volatile pid_t groupList[(MAX_GROUPS+1)][NR_PROCS] = {0};

#ifdef PRINT_IPC_SERVER
#define DEBUG_PRINT(p) p
#else
#define DEBUG_PRINT(p)
#endif

// Function which is invoked by every process while exiting
// to cleanup its information if relavant.
void exit_ipc_cleanup(struct mproc *rmp)
{
    int i, j;

    /* Clean the global data while exiting the process */
    for (i = 0; i < (MAX_GROUPS + 1); i++)
    {
        rmp->mp_procgrps[i] = 0;
    }

    // Clean group table for this process id
    for (i = 1; i < (MAX_GROUPS + 1); i++)
    {
        for (j = 1; j < NR_PROCS; j++)
        {
            if (rmp->mp_pid == groupList[i][j])
            {
                groupList[i][j] = 0;
            }
        }
    }
}

int do_msend(void)
{
    int ii;
    int no_of_senders = 0;
    int gid = m_in.m_user_ipc.gid;
    register struct mproc *rmp = NULL;

    DEBUG_PRINT(printf ("Debug: In Server msend \n"));

    // Check if gid is legitimate
    if (gid <= 0 || gid > MAX_GROUPS)
    {
        DEBUG_PRINT(printf("Debug:In server msend %s:%d\n",__func__, __LINE__));
        return(EINVAL);
    }

    // Check if process is authorized to send to group.
    // This process should be part of group.
    for(ii = 1; ii < NR_PROCS ; ii++)
    {
        DEBUG_PRINT(printf("Debug:In server msend %s:%d\n",__func__, __LINE__));
        if (groupList[gid][ii] == mp->mp_pid)
        {
            DEBUG_PRINT(printf("Debug:In server msend %s:%d\n",__func__, __LINE__));
            break;
        }
    }
    if (ii == NR_PROCS)
    {
        DEBUG_PRINT(printf("Debug:In server msend %s:%d\n",__func__, __LINE__));
        return(EINVAL);
    }

    // Count the number of processes senders in a group
    for(ii = 1; ii < NR_PROCS ; ii++)
    {
        if (groupList[gid][ii] != 0)
        {
            int pid = groupList[gid][ii];
            DEBUG_PRINT(printf("Debug:In server msend %s:%d\n",__func__, __LINE__));
            rmp = find_proc(pid);
            if ((rmp != NULL) && (rmp->sender == 1))
            {
                DEBUG_PRINT(printf("Debug:In server msend %s:%d\n",__func__, __LINE__));
                no_of_senders++;
            }
        }
    }

    // If there are existing senders return -1
    if (no_of_senders > 0)
    {
        DEBUG_PRINT(printf("Debug:In server msend %s:%d\n",__func__, __LINE__));
        return(EPERM);
    }

    // If this is the alone sender, copy the message and set the sender bit
    strcpy (mp->user_msg, m_in.m_user_ipc.user_msg);
    mp->sender = 1;

    return PM_SUCCESS;
}

int do_mreceive(void)
{
    int ii;
    int gid = m_in.m_user_ipc.gid;
    register struct mproc *rmp = NULL;

    DEBUG_PRINT(printf("Debug: In server mreceive\n"));
    // Check if gid is legitimate
    if (gid <= 0 || gid > MAX_GROUPS)
    {
        DEBUG_PRINT(printf("Debug:In server mreceive %s:%d\n",__func__, __LINE__));
        mp->mp_reply.m_pm_lc_message.ret_value = 1;
        return(EINVAL);
    }

    // Check if process belongs to a group.
    // If it doesn't belong then return
    for(ii = 1; ii < NR_PROCS ; ii++)
    {
        if (groupList[gid][ii] == mp->mp_pid)
        {
            DEBUG_PRINT(printf("Debug: In server mreceive found in group id %d\n", gid));
            break;
        }
    }
    if (ii == NR_PROCS)
    {
        DEBUG_PRINT(printf("Debug:In server mreceive %s:%d\n",__func__, __LINE__));
        mp->mp_reply.m_pm_lc_message.ret_value = 1;
        return(EINVAL);
    }

    // Check if there is any sender in a group
    for(ii = 1; ii < NR_PROCS ; ii++)
    {
        if (groupList[gid][ii] != 0)
        {
            int pid = groupList[gid][ii];
            DEBUG_PRINT(printf("Debug:In server mreceive %s:%d\n",__func__, __LINE__));
            rmp = find_proc(pid);
            if ((rmp != NULL) && (rmp->sender == 1))
            {
                DEBUG_PRINT(printf("Debug:In server mreceive %s:%d\n",__func__, __LINE__));
                break;
            }
        }
    }
    DEBUG_PRINT(printf("Debug:In server mreceive %s:%d\n",__func__, __LINE__));

    // No sender yet, keep process blocked
    if (ii == NR_PROCS)
    {
        DEBUG_PRINT(printf("Debug: In server mreceive returning 2\n"));
        mp->mp_reply.m_pm_lc_message.ret_value = 2;
        return PM_SUCCESS;
    }

    // Got the sender, set the received flag
    mp->mp_procgrps[gid] = 1;

    // Return message
    strcpy(mp->mp_reply.m_pm_lc_message.user_msg, rmp->user_msg);
    return PM_SUCCESS;
}

/* Allowd groupID should be range of {1 - MAX_GROUPS} limit
   Out side of this limit group id will be treated as wrong.
   Also, per group number of processes allowd is "NR_PROCS-1".
 */
static int do_createGroup(void)
{
    int ii = 0;
    register struct mproc *rmp = mp;
    DEBUG_PRINT(printf ("Debug: In Server createGroup \n"));

    for (ii = 1; ii <= MAX_GROUPS; ii++)
    {
        if (groupList[ii][0] == 0)
        {
            // Break if first slot of free group is found
            break;
        }
    }

    if (ii > MAX_GROUPS)
    {
        // printf ("Exceeded max group limit of %u \n", (unsigned int )MAX_GROUPS);
        return(EPERM);
    }

    groupList[ii][0] = ii;

    DEBUG_PRINT(printf ("Debug : created group id %d \n", ii));

    return ii;
}


// return : On-Success 0, else errno
int do_closeGroup(void)
{
    int ii = 0;
    int groupID = m_in.m_user_ipc.gid;
    pid_t lpid = m_in.m_user_ipc.pid;

    register struct mproc *rmp = NULL;

    DEBUG_PRINT(printf ("Debug: In Server closeGroup \n"));
    if ((groupID <= 0) || (groupID > MAX_GROUPS))
    {
        DEBUG_PRINT(printf ("Requested group id is not valid\n"));
        return(EINVAL);
    }

    // To remove the complete group.
    if (lpid == 0)
    {
        for (ii = 0; ii < NR_PROCS; ii++ )
        {
            groupList[groupID][ii] = 0;
            mp->mp_reply.m_pm_lc_message.gid = groupID;
        }
    }
    else
    {
        // To detach the process from the group
        if ((rmp = find_proc(lpid)) == NULL)
        {
            return(ESRCH); // invalid process id
        }

        // Valid process, now need to see if its there in group list.
        for(ii = 1; ii < NR_PROCS ; ii++)
        {
            // initial screaning if same pid is getting re-added to same group
            if (groupList[groupID][ii] == rmp->mp_pid)
            {
                groupList[groupID][ii] = (pid_t)0; // detached here
                mp->mp_reply.m_pm_lc_message.gid = groupID;
                mp->mp_reply.m_pm_lc_message.pid = lpid;
                break;
            }
        }
    }
    return PM_SUCCESS;
}


// return : On-Success gid, else errno
// does 2 tasks, create new group if gid passed as 0 else bind with existing group
int do_openGroup(void)
{
    int gid = m_in.m_user_ipc.gid;
    pid_t lpid = m_in.m_user_ipc.pid;
    int ii =0;
    register struct mproc *rmp = NULL;
	DEBUG_PRINT(printf ("Debug: In Server openGroup \n"));

    // passed pid = m_in.m_lsys_pm_getprocnr.pid
    if ((rmp = find_proc(lpid)) == NULL)
    {
        DEBUG_PRINT(printf("Debug: In server openGroup Not valid process\n"));
        return(ESRCH); // invalid process id
    }


    if (gid == 0)
    {
        gid = do_createGroup();
        if (gid == EPERM)
        {
            return(EPERM); // Exceeded max limit
        }
        groupList[gid][1] = lpid;
        mp->mp_reply.m_pm_lc_message.gid = gid;
        return PM_SUCCESS;
    }

    if (gid < 0 || gid > MAX_GROUPS)
    {
        return(EINVAL);
    }

    // ii should start with 1 since 1st one will have
    // group id info, if 0 mean group not yet created.
    for(ii = 1; ii < NR_PROCS ; ii++)
    {
        // initial screaning if same pid is getting re-added to same group
        if (groupList[gid][ii] == rmp->mp_pid)
        {
            break;
        }
    }

    // add to the exsting group
    if (ii == NR_PROCS)
    {
        for(ii = 1; ii < NR_PROCS ; ii++)
        {
            if (groupList[gid][ii] == 0)
            {
                groupList[gid][0] = gid;
                groupList[gid][ii] = lpid;
                mp->mp_reply.m_pm_lc_message.gid = gid;
                DEBUG_PRINT(printf ("Debug: In server openGroup created in gid %d at ii = %d\n", gid, ii));
                break;
            }
        }
    }

    if (ii == NR_PROCS)
    {
        DEBUG_PRINT(printf("Debug: In server openGroup In server openGroup\n"));
        return(EPERM);
    }

    return PM_SUCCESS;
}

int do_recoverGroup(void)
{
    int gid = m_in.m_user_ipc.gid;
    int ii;
    DEBUG_PRINT(printf ("Debug: In Server recoverGroup \n"));
    // Check if gid is legitimate
    if (gid <= 0 || gid > MAX_GROUPS)
    {
        DEBUG_PRINT(printf("Debug:In server mreceive %s:%d\n",__func__, __LINE__));
        mp->mp_reply.m_pm_lc_message.ret_value = 1;
        return(EINVAL);
    }

    for(ii = 1; ii < NR_PROCS ; ii++)
    {
        groupList[gid][ii] = 0;
    }

    return PM_SUCCESS;
}

int do_has_received(void)
{
    int gid = m_in.m_user_ipc.gid;
    int ii;
    int no_of_process = 0;
    register struct mproc *rmp = NULL;

    // Count the number of processes in a group
    for(ii = 1; ii < NR_PROCS ; ii++)
    {
        if (groupList[gid][ii] != 0)
        {
            no_of_process++;
        }
    }

    // Group should atleast have two process, as calling process is part of this group.
    // If there is only one process in group then return without doing anything.
    // As there is no one to receive msg.
    if (no_of_process <= 1)
    {
        DEBUG_PRINT(printf("Debug: In server hasRecieved numprocess less than 1 %s:%d \n", __func__, __LINE__));
        return PM_SUCCESS;
    }

    // Check for all process if they received except sender
    // return -2 and keep it blocked even if single process has not received
    for(ii = 1; ii < NR_PROCS ; ii++)
    {
        if (groupList[gid][ii] != 0)
        {
            int pid = groupList[gid][ii];
            rmp = find_proc(pid);
            if (rmp && (rmp->sender != 1) && (rmp->mp_procgrps[gid] != 1))
            {
                mp->mp_reply.m_pm_lc_message.ret_value = 2;
                DEBUG_PRINT(printf("Debug: In server hasRecieved return 2 %s:%d \n", __func__, __LINE__));
                return PM_SUCCESS;
            }
        }
    }

    // As all process has received message, reset the received flag and sender flag
    for(ii = 1; ii < NR_PROCS ; ii++)
    {
        if (groupList[gid][ii] != 0)
        {
            int pid = groupList[gid][ii];
            rmp = find_proc(pid);
            if (rmp)
            {
                rmp->sender = 0;
                rmp->mp_procgrps[gid] = 0;
            }
        }
    }

    DEBUG_PRINT(printf("Debug: In server hasRecieved %s:%d \n", __func__, __LINE__));
    return PM_SUCCESS;
}
