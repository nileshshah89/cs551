#include <sys/cdefs.h>
#include "namespace.h"
#include <lib.h>

#include <string.h>
#include <unistd.h>
#include <stdio.h>

#ifdef PRINT_IPC_LIBC
#define DEBUG_PRINT(p) p
#else
#define DEBUG_PRINT(p)
#endif

int recoverGroup(int gid)
{
    message m;
    int ret = 0;
    DEBUG_PRINT(printf ("Debug: In libc recoverGroup\n"));
    memset(&m, 0, sizeof(m));
    m.m_user_ipc.gid = gid;

    ret = _syscall(PM_PROC_NR, PM_RECOVERGROUP, &m);

    if (ret != IPC_SUCCESS)
    {
        return IPC_FAIL;
    }

    return IPC_SUCCESS;
}
