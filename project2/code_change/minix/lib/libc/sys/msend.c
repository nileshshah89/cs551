#include <sys/cdefs.h>
#include "namespace.h"
#include <lib.h>

#include <string.h>
#include <unistd.h>
#include <stdio.h>

#ifdef PRINT_IPC_LIBC
#define DEBUG_PRINT(p) p
#else
#define DEBUG_PRINT(p)
#endif

// This function will poll till all receivers
// receives messages
static void checkIfMessageReceived(int gid)
{
    message m;
    int result = 0;

    DEBUG_PRINT(printf ("Debug: In checkIfMessageReceived\n"));

    while (1)
    {
        memset(&m, 0, sizeof(m));

        // Store groupid in message structure
        m.m_user_ipc.gid = gid;

        // Invoke process manager
        _syscall(PM_PROC_NR, PM_HAS_RECEIVED, &m);

        // If received message is 2, sleep for a sec
        // and then continue
        if (m.m_pm_lc_message.ret_value == 2)
        {
            sleep(1);
        }
        else
        {
            break;
        }
    }
}

int msend(int gid, char *msg)
{
    message m;
    int result = 0;

    // Check if msg pointer is NULL
    if (msg == NULL) return IPC_FAIL;

    // Check the length of message
    int len = strlen(msg);

    // If length of message is greater than max length
    // then return
    if (len > MAX_MESSAGE_LENGTH)
    {
        printf("Error: Message length exceeded %d\n", MAX_MESSAGE_LENGTH);
        return IPC_FAIL;
    }

    memset(&m, 0, sizeof(m));

    DEBUG_PRINT(printf ("Debug: In libc msend\n"));

    // Copy the message to message structure
    strcpy(m.m_user_ipc.user_msg, msg);

    // Store gid and pid in message structure
    m.m_user_ipc.gid = gid;
    m.m_user_ipc.pid = getpid();

    // Invoke process message
    result = _syscall(PM_PROC_NR, PM_MSEND, &m);

    // If something went wrong, return
    if (result != IPC_SUCCESS)
    {
        return IPC_FAIL;
    }

    // Poll if everything went right
    checkIfMessageReceived(gid);

    return IPC_SUCCESS;
}
