#include <sys/cdefs.h>
#include "namespace.h"
#include <lib.h>

#include <string.h>
#include <unistd.h>
#include <stdio.h>

#ifdef PRINT_IPC_LIBC
#define DEBUG_PRINT(p) p
#else
#define DEBUG_PRINT(p)
#endif

int mreceive(int gid, char *msg)
{
    message m;

    DEBUG_PRINT(printf ("Debug: In libc mreceive\n"));

    if (msg == NULL) return IPC_FAIL;
    while(1)
    {
        memset(&m, 0, sizeof(m));

        // Store gid in message structure
        m.m_user_ipc.gid = gid;

        // Invoke Process manager
        _syscall(PM_PROC_NR, PM_MRECEIVE, &m);

        // If something went unexpected, return
        if (m.m_pm_lc_message.ret_value == 1)
        {
            return IPC_FAIL;
        }

        // If need to pool, sleep for a second
        // and then continue
        if (m.m_pm_lc_message.ret_value == 2)
        {
            sleep(1);
        }

        // Copy received msg and send back to user
        else
        {
            strcpy(msg, m.m_pm_lc_message.user_msg);
            return IPC_SUCCESS;
        }
    }
}
