#include <sys/cdefs.h>
#include "namespace.h"
#include <lib.h>

#include <string.h>
#include <unistd.h>
#include <stdio.h>

#ifdef PRINT_IPC_LIBC
#define DEBUG_PRINT(p) p
#else
#define DEBUG_PRINT(p)
#endif

int openGroup(int gid, pid_t lpid)
{
    message m;
    int ret = 0;
    DEBUG_PRINT(printf ("Debug: In libc openGroup\n"));

    memset(&m, 0, sizeof(m));

    // If lpid is zero, feed the own pid
    if (lpid == 0)
    {
        lpid = getpid();
    }

    // Store lpid and gid in message
    m.m_user_ipc.pid = lpid;
    m.m_user_ipc.gid = gid;

    // Invoke process manager
    ret = (_syscall(PM_PROC_NR, PM_OPENGROUP, &m));

    if (ret != IPC_SUCCESS)
    {
        DEBUG_PRINT(printf("openGroup failed %d \n", ret));
        return IPC_FAIL;
    }

    // Return git
    return m.m_pm_lc_message.gid;
}
