#include <sys/cdefs.h>
#include "namespace.h"
#include <lib.h>

#include <string.h>
#include <unistd.h>
#include <stdio.h>

#ifdef PRINT_IPC_LIBC
#define DEBUG_PRINT(p) p
#else
#define DEBUG_PRINT(p)
#endif

int closeGroup(int gid, pid_t lpid)
{
    message m;
    int ret = 0;

    DEBUG_PRINT(printf ("Debug: In libc closeGroup\n"));

    memset(&m, 0, sizeof(m));

    // Store lpid and gid in message structure
    m.m_user_ipc.pid = lpid;
    m.m_user_ipc.gid = gid;

    // Invoke process manager
    ret = _syscall(PM_PROC_NR, PM_CLOSEGROUP, &m);

    // If something went wrong, return
    if (ret != IPC_SUCCESS)
    {
        DEBUG_PRINT(printf ("Closed Group failed  - %d \n", ret));
        return IPC_FAIL;
    }

    return IPC_SUCCESS;
}
