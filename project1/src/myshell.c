#include "main.h"
#include "myshell.h"
#include "parseAndExecuteCommand.h"

void startShell()
{
    char commandLine[SIZE];

    while (1)
    {
        printCurrentDirector();
        getCommand(commandLine);
        parseAndExecuteCommand(commandLine);
    }
}

/* @Function Name:
 * @brief:
 * @return:
 */
void printCurrentDirector() {
    char cwd[SIZE];
    int size;
    int i = 0;

    // Ask is to just print current directory
    // and not the absolute path, thus traverse
    // from the end of the array to the last forward
    // slash and then increment the pointer by 1 so that
    // it points to the right current directory.

    // Get current working directory
    getcwd(cwd, SIZE);

    // Get the size of array
    size = strlen(cwd);

    // Traverse till last forward slash from the end of the
    // array.
    while (cwd[--size] != '/');

    // Currently, size is pointing to the last forward slash,
    // increment it by one so that it points to current directory
    size++;

    // Print the current working directory
    printf("%s # ", &cwd[size]);

    return;
}

/* @Function Name:
 * @brief:
 * @return:
 */
void getCommand(char *commandLine) {
    char c = '\0';
    UINT i = 0;

    fflush(stdin);
    fflush(stdout);
    c = fgetc(stdin);
    while ( c != '\n') {
        commandLine[i++] = c;
        c = fgetc(stdin);
    }

    // Gracefull exit if exceeded the char limit. here we shouldn't ignore
    // the leading or trailing whitespace for robustness, since user choose to
    // provide those character, and should honour their inputs.
    if (i < SIZE)
    {
        commandLine[i] = '\0';
    }
    else
    {
        LOGE ("Shell Exit, limit exceeded [%u] - GOODBYE!\n", SIZE);
        exit(0);
    }
}
