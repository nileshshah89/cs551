#include "precedence.h"

/*Return 1 if expression has balanced Parentheses */
BOOL areParenthesesBalanced(char expression[], int *commandDepth)
{
    int i = 0;
    int depth = 0;
    int localDepth = 0;
 
   /* Declare an empty character stack */
    struct parenthesesNode *pStack = NULL;
 
    /* Traverse the given expression to check matching parentheses */
    while (expression[i])
    {

        /*If the expression[i] is a starting parentheses then push it*/
        if (expression[i] == '{' || expression[i] == '(' || expression[i] == '[')
        {
            push(&pStack, expression[i]);
            localDepth++;
            if (depth < localDepth)
            {
                depth++;
            }
        }
 
        /* If expression[i] is a ending parentheses then pop from stack and 
            check if the popped parentheses is a matching pair*/
        if (expression[i] == '}' || expression[i] == ')' || expression[i] == ']')
        {
            /*If we see an ending parentheses without a pair then return false*/
            if (NULL == pStack)
                return FALSE; 
 
            /* Pop the top element from stack, if it is not a pair 
              parentheses of character then there is a mismatch.
              This happens for expressions like {(}) */
            else if ( !isMatchingPair(pop(&pStack), expression[i]) )
                return FALSE;

            localDepth--;
        }
        i++;
    }
     
    /* If there is something left in expression then there is a starting
       parentheses without a closing parentheses */
    if (NULL == pStack)
    {
        *commandDepth = depth;
        return TRUE; /*balanced*/
    }
    else
        return FALSE;  /*not balanced*/
} 

/* Returns 1 if character1 and character2 are matching left
   and right Parentheses */
BOOL isMatchingPair(char character1, char character2)
{
    if (character1 == '(' && character2 == ')')
        return TRUE;
    else if (character1 == '{' && character2 == '}')
        return TRUE;
    else if (character1 == '[' && character2 == ']')
        return TRUE;
    else
        return FALSE;
}

/* Function to push an item to stack*/
void push(struct parenthesesNode** parentNode, int new_data)
{
    /* allocate new node */
    struct parenthesesNode* new_node =
              (struct parenthesesNode*) malloc(sizeof(struct parenthesesNode));
 
    if (NULL == new_node)
    {
        printf("Stack overflow \n");
        getchar();
        exit(0);
    }           
 
    /* put in the data  */
    new_node->data  = new_data;
 
    /* link the old list off the new node */
    new_node->next = (*parentNode);  
 
    /* move the head to point to the new node */
    (*parentNode)    = new_node;
}
 
/* Function to pop an item from stack*/
int pop(struct parenthesesNode** parentNode)
{
    char element;
    struct parenthesesNode *top;
 
    /*If stack is empty then error */
    if (NULL == *parentNode)
    {
        printf("Stack overflow \n");
        getchar();
        exit(0);
    }
    else
    {
        top = *parentNode;
        element = top->data;
        *parentNode = top->next;
        free(top);
        return element;
    }
}
