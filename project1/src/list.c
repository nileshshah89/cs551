/*
 * @file list.c
 *
 * @brief Generic implementation of linked list function.
 *
 * @author Vijay  Gupta (A20330677)
 *         Nilesh Shah  ()
 *         Kiran        ()
 *
 */

#include "list.h"

// To debug linked list if things go wrong.
// #define LIST_DEBUG

#ifdef LIST_DEBUG
void prtstr(void *str)
{
    LOGD (" \"%s\" \n", (char *)str);
}

BOOL valfind (void *val, void *str)
{
    char* temp = (char *)str;
    if (strcmp ((char*)val, temp) == 0)
        return TRUE;

    return FALSE;
}

int valdelete(void * val, void *key)
{
    char* temp = (char *)key;
    if (strcmp ((char*)val, temp) == 0)
    {
        LOGD ("Deleted %s\n", (char*)val);
        return TRUE;
    }

    return FALSE;
}

void cleanup(void *val)
{
    // free (val);
}
#endif

void insertList (List **p, void *pVal)
{
    List *p1    = NULL;

    p1 = (List *) malloc(sizeof(List));
    if (p1 == NULL)
    {
        LOGE ("OOM-error \n");
        return;
    }

    p1->pVal    = pVal; // allocate in use-case itself pVal
    p1->pNext   = *p;  // for the first time *p should be null.
    *p          = p1; // insert always as first element
}

void showList (List *p, void (*fPtr)(void *))
{
    while (p != NULL)
    {
        if (p->pVal != NULL)
            (*fPtr)(p->pVal);  // add the print in usecase itself
        p = p->pNext;
    }
    return;
}

void *findVal (List *p, BOOL (*fPtr)(void *, void *), void *key)
{
    BOOL flag = FALSE;

    if ((*fPtr) == NULL) return (void *) NULL;

    while (p != NULL)
    {
        flag = (*fPtr)(p->pVal, key); // should return true if matched
        if (flag == TRUE)
        {
            return (void *)p->pVal;
        }
        p = p->pNext;
    }

    return (void *)NULL;
}

void deleteVal (List **p, BOOL (*fPtr)(void *, void *), void *key)
{
    BOOL flag = FALSE;
    List *temp = *p, *prev;

    if ((*fPtr) == NULL) return;
    // if key element is in head node itself
    // deep cleaning required in use-case itself.
    if ((temp != NULL) && ((*fPtr)(temp->pVal, key) == TRUE))
    {
        *p = temp->pNext;
        free (temp);
        return;
    }

    while ((temp != NULL) && ((*fPtr)(temp->pVal, key) != TRUE))
    {
        prev = temp;
        temp = temp->pNext;
    }

    if (temp == NULL) return;

    prev->pNext = temp->pNext;

    free (temp);
}

void deleteList (List **p, void (*fPtr)(void *))
{
    List *temp = *p, *prev;

    while (temp != NULL)
    {
        prev = temp;
        (*fPtr)(temp->pVal); // client should destroy their allocated memory under this function
        temp = temp->pNext;
        free (prev);
    }

    *p = NULL;
}

#ifdef LIST_DEBUG
int main(void)
{
    List *x = NULL, *y = NULL; // PS : NULL Initialization critical here.
    void *p1;
    char *str [] = {"vijay", "Nilesh", "HELLO", "RED", "BLUE"};

    insertList (&x, str[0]);
    insertList (&x, str[1]);
    insertList (&x, str[2]);
    insertList (&x, str[3]);
    insertList (&x, str[4]);
    y = x;

    showList(y, prtstr);

    y = x;
    p1 = findVal (y, valfind, (void *)"HELLO");

    if (p1 != NULL)
        LOGE ("string : %s \n", (char *)p1);

    deleteVal(&x, valdelete, (void *)"HELLO");

    y = x;
    showList(y, prtstr);
    deleteList (&x, cleanup);
    y = x;
    showList(x, prtstr);

    return 1;
}
#endif
