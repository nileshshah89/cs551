/*
 * @file  main.c
 *
 * @brief myshell main function implementation.
 *
 * @author Vijay  Gupta (A20330677)
 *         Nilesh Shah  (A20324149)
 *         Kiran Suresh (A20316940)
 */

#include "main.h"
#include "utils.h"
#include "alias.h"
#include "parseAndExecuteCommand.h"
#include "signals.h"
#include "myshell.h"

// Prototype for functions in main()
void parseProfileFile();
void changeToHomeDirectory();

char HOME[SIZE];

/* @Function Name: main
 * @brief: main function for myshell
 * @return:
 */
int main() {
    parseProfileFile();
    changeToHomeDirectory();
    installSignalHandlers();
    if (createAliasList() != TRUE)
    {
        LOGD ("Fail to prepare alias list \n");
    }

    startShell();
    return 0;
}

/* @Function Name: parseProfileFile
 * @brief: Parse profile file for setting home variable
 * @return: None
 */
void parseProfileFile() {
    FILE *fd;
    char homeVariable[PROFILE_VARIABLE_SIZE];
    int i = 0, j = 0;

    // Change directory to root
    chdir ("/home/nsshah89");

    // Open the profile file
    fd = fopen("./PROFILE", "r");
    if (fd == NULL) {
        LOGE ("PROFILE file not found [error : \"%s\"] \n", strerror(errno));
        exit(-1);
    }

    // Read the first line of PROFILE file
    fgets(homeVariable, PROFILE_VARIABLE_SIZE, fd);
    if (strncmp ("HOME=", homeVariable, strlen("HOME=")) != 0)
    {
        LOGE ("Invalid variable name in profile file, expected =%s \n", "HOME=");
        exit(-1);
    }

    LOGD ("Home variable: %s", homeVariable);

    // Move till '=' symbol
    i = 0;
    while (homeVariable[i++] != '=');

    // Save rest of the line into the global HOME variable and append null character
    j = 0;
    while ((homeVariable[i] != '\n') && (homeVariable[i] != '\0')) {
        HOME[j++] = homeVariable[i++];
    }
    HOME[j] = '\0';

    LOGD ("Home: %s, length HOME = %u\n", HOME, strlen(HOME));

    // Close fd for profile file
    fclose(fd);

    return;
}

/* @Function Name: changeToHomeDirectory
 * @brief: functio to move to home dir
 * @return: None
 */
void changeToHomeDirectory() {
    char cwd[SIZE];
    chdir (HOME);
    getcwd(cwd, SIZE);
    return;
}
