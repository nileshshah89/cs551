#include "main.h"
#include "myshell.h"
#include "alias.h"
#include "precedence.h"
#include "parseAndExecuteCommand.h"

static commandNode * parseExpression (char *commandLine);
static void invokeExecute (commandNode *parsedCmdLine, int waitForChildCompletion);
static char * getAndRemoveCommandForDepth (char *command, int depth, int *waitForChildCompletion);

extern char HOME[SIZE];
/* @Function Name:
 * @brief:
 * @return:
 */
void parseAndExecuteCommand (char *commandLine)
{
    BOOL result;
    int depth = 0;
    commandNode *parsedCmdLine;

    result = areParenthesesBalanced(commandLine, &depth);
    if (result == FALSE)
    {
        printf("Error: Unbalanced expression\n");
        return;
    }

    // No parenthesis in expression execute commands from left to right
    if (depth == 0)
    {
        int waitForChildCompletion = 1;
        parsedCmdLine = parseExpression (commandLine);
        invokeExecute (parsedCmdLine, waitForChildCompletion);
    }

    // There are parenthesis in expression, shuffle the commands
    else
    {
        char copyofCommandLine[SIZE];
        char *start;

        strcpy(copyofCommandLine, commandLine);
        start = copyofCommandLine;

        while (depth > 0)
        {
            while (1)
            {
                char *commands = NULL;
                int waitForChildCompletion = 1;
                commands = getAndRemoveCommandForDepth(start, depth, &waitForChildCompletion);
                if (commands == NULL)
                {
                    break;
                }
                parsedCmdLine = parseExpression (commands);
                invokeExecute (parsedCmdLine, waitForChildCompletion);
                free (commands);
            }
            depth--;
        }

        if (strlen(start) > 0) {
            int waitForChildCompletion = 1;
            parsedCmdLine = parseExpression (start);
            invokeExecute (parsedCmdLine, waitForChildCompletion);
        }
    }
    return;
}

static char * getAndRemoveCommandForDepth (char *command, int depth, int *waitForChildCompletion) {
    if (strlen(command) < 1) {
        return NULL;
    }
    else {
        char *temp;
        int count = 0;
        int i = 0, j = 0;
        int startParenthesis = 0;
        int endParenthesis = 0;
        int ParenthesisFound = 0;

        while (command[i])
        {
            if (command[i] == '(')
            {
                count++;
            }
            if (command[i] == ')')
            {
                count--;
            }

            if (count == depth)
            {
                startParenthesis = i;
                ParenthesisFound = 1;
                // Point to next character after parenthesis
                i++;
                break;
            }
            i++;
        }

        if (ParenthesisFound == 0) {
            return NULL;
        }

        temp = (char *) malloc (sizeof(char) * SIZE);
        if (temp == NULL)
        {
            printf("Error: Malloc failed\n");
            exit(-1);
        }

        // Copy commands which are between parenthesis
        while (command[i] != ')')
        {
            temp[j++] = command[i++];
        }

        // point to character next to closing parenthesis
        i++;
        endParenthesis = i;
        temp[j] = '\0';

        // Remove the copied string from main commandline
        while ((command[startParenthesis++] = command[endParenthesis++]) != '\0');

        // Check for mulitple commands in the current command
        // This can be done by checking if there is ,
        i = 0;
        while (temp[i]) {
            if (temp[i] == ',')
            {
                *waitForChildCompletion = 0;
                break;
            }
            i++;
        }

        return temp;
    }
}
static void invokeExecute (commandNode *parsedCmdLine, int waitForChildCompletion) {
   commandNode *temp = NULL;
   temp = parsedCmdLine;

   // Execute commands
   while (parsedCmdLine != NULL) {
       executeCommand (parsedCmdLine->command, waitForChildCompletion);
       parsedCmdLine = parsedCmdLine->next;
   }

   // Free parsedCmdLine
   while (temp != NULL) {
       commandNode *t = temp->next;
       free (temp->command);
       free (temp);
       temp = t;
   }
}
static commandNode * parseExpression (char *commandLine)
{
    commandNode *start = NULL, *next = NULL;
    commandNode *temp =  NULL;
    char t[SIZE];
    int i = 0, j = 0;

    /* Eg: input = echo 1, echo 2, echo 3
     * output [echo 1] -> [echo 2] -> [echo 3] -> NULL
     * [] -> commandNode*
     */
    if (strlen(commandLine) > 0)
    {
        // Allocate a node
        start = (commandNode *) malloc (sizeof(commandNode));
        start->command = (char *) malloc (sizeof(char) * SIZE);
        start->next = NULL;

        temp = start;
        // Parse till end of the line
        while (commandLine[j] != '\0')
        {
            // Command seperator is" ,". Look for comma and seperate it.
            if ((t[i++] = commandLine[j++]) == ',')
            {
                // Replace , by null character
                t[--i] = '\0';

                // Allocate new node
                commandNode * kk = (commandNode *) malloc (sizeof(commandNode));
                kk->command = (char *) malloc (sizeof(char) * SIZE);
                kk->next = NULL;

                // Adjust the structures
                strcpy(temp->command, t);
                temp->next = kk;
                temp = kk;
                i = 0;
            }
        }
        t[i] ='\0';
        strcpy(temp->command, t);
    }
    return start;
}

/* @Function Name:
 * @brief:
 * @return:
 */
void executeCommand(char *cmdline, int waitForChildCompletion) {
    int i = 0, j = 0, k = 0;
    char temp[SIZE];
    char **argv = NULL;
    int donotCreateChild = 0;
    void *pTempNode = NULL;

    // Allocate memory
    if (argv == NULL) {
        argv = (char **) malloc (sizeof(char *) * MAX_COMMANDS);
        if (argv == NULL) {
            LOGE("Error: Malloc failed: %d\n", __LINE__);
            exit (-1);
        }

        for (i = 0; i < MAX_COMMANDS; i++) {
            argv[i] = (char *) malloc (sizeof(char) * SIZE);
            if (argv[i] == NULL) {
                LOGE("Error: Malloc failed: %d\n", __LINE__);
                exit (-1);
            }
        }
    }

    if (cmdline != NULL)
    {
        i = 0, j = 0;
        // Clear off all the leading white spaces
        while (cmdline[i] == ' ') i++;

        if (i > 0)
        {
            while ((cmdline[j++] = cmdline[i++]) != '\0');
        }

        pTempNode = getAliasObjByKey((void*)cmdline);
        if ( pTempNode != NULL)
        {
            char *temp = getAliasVal(pTempNode);
            if (temp != NULL)
            {
                strcpy(cmdline, temp);
            }
        }
    }

    // Parse each command and place each
    // token into seperate string with null termination
    // example:
    //          $ cat file1 file2
    // will be converted to
    // argv[0] = cat
    // argv[1] = file1
    // argv[2] = file2
    // argv[3] = NULL
    // NULL termination is requirement of execvp

    i = 0; j = 0; k = 0;

    while (cmdline[i] != '\0')
    {
        j = 0;
        // Clear off all the leading white spaces
        while (cmdline[i] == ' ') i++;

        // Break if new line is encountered followed by blank spaces
        if (cmdline[i] == '\0') break;

        while ( (cmdline[i] != ' ') &&
                (cmdline[i] != '\0') &&
                (cmdline[i] != '"'))
        {
            temp[j++] = cmdline[i++];
        }

        switch (cmdline[i]) {
            case '"':
                i++;
                while ((cmdline[i] != '"') &&
                       (cmdline[i] != '\0'))
                {
                    temp[j++] = cmdline[i++];
                }
            case ' ':
            case '\0':
                temp[j] = '\0';
                strcpy(argv[k], temp);
                k++;
                break;
        }
        if (cmdline[i] == '\0') break;
        i++;

    }

    // if k = 0, it means there were no command, those were plane white spaces
    if (k == 0)
    {
        return;
    }
    // Must be null terminated .
    // *(argv + k) = NULL;
    argv[k] = NULL;

    if (argv[0] != NULL)
    {
        i = 0, j = 0;

        pTempNode = getAliasObjByKey((void*)argv[0]);
        if ( pTempNode != NULL)
        {
            char *temp = getAliasVal(pTempNode);
            if (temp != NULL)
            {
                strcpy(argv[0], temp);
            }
        }
    }

    // TO support Alias we need, token in format
    // added alias entry in alias list.
    if (strcmp (argv[0], "alias") == 0)
    {
        if (argv[1] == NULL)
        {
            showAllAlias();
        }
        else
        {
            int ii = 1;
            while (argv[ii] != NULL)
            {
                if (insertAliasInList((void *)argv[ii]) == FALSE)
                {
                    LOGE("alias not found : [%s] \n", argv[ii]);
                }
                ii++;
            }
        }
        donotCreateChild = 1;
    }

    // Handle cd command
    if (strcmp(argv[0], "cd") == 0) {
        changeDirectory (argv);
        donotCreateChild = 1;
    }

    // Handle exit command
    else if (strcmp(argv[0], "exit") == 0) {
        closeAliasList();
        exit (0);
    }

    // Create a child only if donotCreateChild is not set
    if (donotCreateChild == 0) {
        createChild(argv, waitForChildCompletion);
    }

    // @to-do optimize it
    // Free allocated memory
    for (i = 0; i < MAX_COMMANDS; i++) {
        free (argv[i]);
    }
    free (argv);
}

void createChild(char **argv, int waitForChildCompletion) {
    pid_t mypid = 0;
    int status;

    mypid = fork();

    if (mypid == 0) {
        // Child process
        execvp(argv[0], (char **) argv);
        LOGE("%s : mybash : command not found \n", (char *) argv[0]);
    }
    else {
        // Parent process
        // exec will return -1 only when it has incorrect commands
        if (waitForChildCompletion == 1) {
            wait(&status);
        }
        return;
    }
}

void changeDirectory(char **argv) {
    char cwd[SIZE];
    int result = 0;

    cwd[0] = '\0';

    // Handle these scenario
    // 1. cd
    // 2. cd ../../..
    // 3. cd relative path from current working directory
    // 4. cd absolute path

    // Scenario 1. cd
    // Just cd will change current directory to home directory
    if (argv[1] == NULL) {
        strcpy (cwd, HOME);
    }

    // Scenario 2 and 3
    else if (argv[1][0] != '/') {
        // Get current working directory
        getcwd(cwd, SIZE);

        // Append forward slash to current working directory and
        // then with next argument.
        strcat(cwd, "/");
        strcat(cwd, argv[1]);
    }

    // Scenario 4 Absolute path
    else if (argv[1][0] == '/') {
        strcpy(cwd, argv[1]);
    }

    // Change directory
    result = chdir(cwd);
    if (result != 0) {
        printf("Error, No such directory\n");
    }
    return;
}
