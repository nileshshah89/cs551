/*
 * @file 
 *
 * @brief 
 *
 * @author Vijay  Gupta, Nilesh Shah, Kiran
 *
 */

#include "alias.h"
#include "signals.h"
#include <signal.h>

void installSignalHandlers()
{
    struct sigaction sa;

    sa.sa_handler = &handleSignal;
    sa.sa_flags = SA_RESTART;
    sigfillset(&sa.sa_mask);

    //Handle SIGHUP
    if (sigaction(SIGHUP, &sa, NULL) == -1)
    {
        perror("Error: cannot handle SIGHUP");
    }

    //Handle SIGINT
    if (sigaction(SIGINT, &sa, NULL) == -1)
    {
        perror("Error: cannot handle SIGINT");
    }

    //Handle SIGTERM
    if (sigaction(SIGTERM, &sa, NULL) == -1)
    {
        perror("Error: cannot handle SIGTERM");
    }

    //Handle SIGUSR1 -- hook for future use
    if (sigaction(SIGUSR1, &sa, NULL) == -1)
    {
        perror("Error: cannot handle SIGUSR1");
    }

    //Handle SIGUSR1 -- hook for future use
    if (sigaction(SIGQUIT, &sa, NULL) == -1)
    {
        perror("Error: cannot handle SIGUSR1");
    }


	// Dont handle SIGQUIT to generate abort core
}

/* @Function Name: handleSignal
 * @brief: Handles interupted signals
 * @return: void
 */
void handleSignal(int signalNum)
{
    char str;
    sigset_t pending;

    switch (signalNum)
    {
        case SIGHUP:
            break;

    case SIGQUIT:
        case SIGINT:
	case SIGTERM:
            printf("\nAre you sure? (y/n): ");
            scanf("%c", &str);
            if ('Y' == str || 'y' == str)
            {
                //exit shell
                printf("Exiting shell - Goodbye!\n\n");

                closeAliasList();
                exit(0);
            }
            break;

        case SIGUSR1:
            break;
    }

    sigpending(&pending);
    if (sigismember(&pending, SIGHUP))
    {
        printf("A SIGHUP is waiting\n");
    }
    if (sigismember(&pending, SIGUSR1))
    {
        printf("A SIGUSR1 is waiting\n");
    }
}
