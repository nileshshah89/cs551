/*
 * @file alias.c
 *
 * @brief implement alias feature for shell.
 *
 * @author Vijay Gupta  (A20330677)
 *         Nilesh Shah  (A20324149)
 *         Kiran Suresh (A20316940)
 */
#include "main.h"
#include "alias.h"

/* Max alias length */
#define MAX_ALIAS_LEN 100

// Head node of alias list
List *pAliasListHead = NULL;

extern char HOME[SIZE];

/* Start Of - alias utility functions */

/* Utility function to write list content in file for persitent storage */
static BOOL writeInFile(const char *key, const char *val, FILE *fd)
{
    UINT ret;

    if ((key == NULL) || (val == NULL))
    {
        LOGD("Failed-to-write-in-File: Invalid argument \n");
        return FALSE;
    }

    ret  = fwrite(key, 1, strlen(key), fd);
    ret += fwrite("=", 1, 1,fd);
    ret += fwrite(val, 1, strlen(val), fd);
    ret += fwrite("\n", 1, 1, fd);

    if(ret != (strlen(key) + 1 + strlen(val)+1)) { return FALSE; }

    return TRUE;
}

/* Utility function to deep cleaning the alias list */
static void freeNode (void *ptr)
{
    aliasListSt *pAliasListSt = (aliasListSt *)ptr;
    if (pAliasListSt != NULL)
    {
        // freeing key and val allocation
        if (pAliasListSt->key != NULL) { free (pAliasListSt->key); }
        if (pAliasListSt->val != NULL) { free (pAliasListSt->val); }

        pAliasListSt->key = NULL;
        pAliasListSt->val = NULL;

        // freeing alias lis allocation
        free (pAliasListSt);
        pAliasListSt = NULL;

    }
}

/* Utility function to add alias entry */
static BOOL addAliasEntry (char *key, char *val)
{
    aliasListSt *pAliasListSt = NULL;

    if ((key == NULL) || (val == NULL)) { return FALSE; }

    pAliasListSt = (aliasListSt *) malloc (sizeof (aliasListSt));
    if (pAliasListSt == NULL)
    {
        LOGE("%s:%d - OOM \n", __func__, __LINE__);
        return FALSE;
    }

    // Add alias entry to list node
    pAliasListSt->key = key;
    pAliasListSt->val = val;

    // insert node to alias list
    insertList (&pAliasListHead, (void *)pAliasListSt);

    return TRUE;
}

/* Utility function to show each alias entry */
static void showAliasEntry(void *ptr)
{
    if (ptr == NULL) { return; }
    aliasListSt *pAliasListSt = (aliasListSt *)ptr;
    LOGE ("alias %s=%s \n", pAliasListSt->key, pAliasListSt->val);
}

/* To check if particular key exist in alias list */
static BOOL findAliasKey (void *ptr, void *key)
{
    aliasListSt *pAliasListSt = (aliasListSt *)ptr;
    if (strcmp ((char *)pAliasListSt->key, (char *)key) == 0)
        return TRUE;

    return FALSE;
}

/* To check if particular val exist in alias list */
static BOOL findAliasVal (void *ptr, void *val)
{
    aliasListSt *pAliasListSt = (aliasListSt *)ptr;
    if (strcmp ((char *)pAliasListSt->val, (char *)val) == 0)
        return TRUE;
    return FALSE;
}


/* End Of - alias utility functions */

/* @Function Name: createAliasList
 * @brief: Create alias list during shell launch
 * @return: True on Success, Otherwise False
 */
BOOL createAliasList (void)
{
    BOOL ret = TRUE;
    FILE *fd = NULL;
    char str[MAX_ALIAS_LEN] = {0, };
    char *token1 = NULL;
    char *token2 = NULL;
    char *key = NULL;
    char *val = NULL;
    char filepath[SIZE];

    strcpy(filepath, HOME);
    strcat(filepath, "/");
    strcat(filepath, ALIAS_FILE);
    fd = fopen (filepath, "a+");
    if (fd == NULL)
    {
        LOGE ("alias failed - [error : \"%s\"] \n", strerror(errno));
        return FALSE;
    }

    while (fgets (str, MAX_ALIAS_LEN, fd) != NULL)
    {
#ifdef DEBUG_ALL
        //        puts (str);
#endif

        /* Create token based on '=', so that could add in key or in value */
        // remove '\n' char from string
        str[strlen(str)-1] = '\0';
        if (str == NULL) { return ret;}

        token1 = strtok (str, "=");
        token2 = strtok (NULL, "=");
        if ((token1 != NULL) &&  (token2 != NULL))
        {
            key = (char *) malloc (strlen(token1)+1);
            val = (char *) malloc (strlen(token2)+1);
            if ((key != NULL) && (val != NULL))
            {
                strncpy(key, token1, strlen(token1));
                strncpy(val, token2, strlen(token2));

                // tokens are not NULL terminated
                key[strlen(token1)] = '\0';
                val[strlen(token2)] = '\0';

            }
            else
            {
                if ((key == NULL) && (val == NULL)) { LOGE("%s:%d - OOM \n", __func__, __LINE__); }
                else if (key) { free(key);}
                else { free(val); }
                LOGE("%s:%d - OOM \n", __func__, __LINE__);
                ret = FALSE;
            }
        }
        else
        {
            LOGE ("Incorrect token -> [%s] \n", str);
            ret = FALSE;
        }

        if (ret != FALSE)
        {
            LOGD ("%s:%d : KEY = [%s] : VAL = [%s] \n", __func__, __LINE__, key, val);
            addAliasEntry (key, val);
        }

        // re-initialize the local pointers
        key = NULL;
        val = NULL;
        token1 = NULL;
        token2 = NULL;

    }

    if ((pAliasListHead != NULL) && (ret == FALSE))
    {
        LOGD("%s:%d : Alias created partialy, invalid input \n", __func__, __LINE__);
        ret = TRUE;
    }

    fclose(fd);
    return ret;
}

/* @Function Name: storeAliasList
 * @brief: Store alias list to file for permanent storage
 * @return: True on Success, Otherwise False
 */
BOOL storeAliasList (void)
{
    FILE *fd = NULL;
    List *pTemp = pAliasListHead;
    aliasListSt *pAliasListSt = NULL;
    char filepath[SIZE];

    strcpy(filepath, HOME);
    strcat(filepath, "/");
    strcat(filepath, ALIAS_FILE);


    fd = fopen (filepath, "w");
    if (fd == NULL)
    {
        LOGE ("alias failed - [error : \"%s\"] \n", strerror(errno));
        return FALSE;
    }

    while (pTemp != NULL)
    {
        pAliasListSt = (aliasListSt *)pTemp->pVal;
        if (writeInFile(pAliasListSt->key, pAliasListSt->val, fd) == FALSE)
        {
            LOGE ("Failed to write in-File for alias : [%s] \n", pAliasListSt->key);
        }

        pTemp = pTemp->pNext;
    }

    fclose (fd);

    return TRUE;

}

/* @Function Name: deleteAliasList
 * @brief: delete alias list, deep cleaning
 * @return: None
 */
void deleteAliasList (void)
{
    /* delete complete list */
    deleteList(&pAliasListHead, (void*)freeNode);
}

/* @Function Name: showAllAlias
 * @brief: traverse complete list to show list entry
 * @return: None
 */
void showAllAlias(void)
{
    List *temp = pAliasListHead;
    showList(temp, showAliasEntry);
}

/* @Function Name: showAllAlias
 * @brief: to validate if alias and corresponding value exist in list
 * @return: True on Success, Otherwise False
 */
BOOL isAliasExist(void *key, void *val)
{

    List *temp = pAliasListHead;
    void *f = findVal (temp, findAliasKey, (void *)key);
    if ( f != NULL)
    {
        if (((findAliasKey(f, key)) == TRUE) && ((findAliasVal(f, val)) == TRUE)) { return TRUE; };
    }
    return FALSE;
}

/* @Function Name: getAliasObjByKey
 * @brief: func to pull alias object from the list
 * @return: Object handle or NULL
 */
void *getAliasObjByKey(const void *ptr)
{
    List *temp = pAliasListHead;
    void *f = findVal (temp, findAliasKey, (void *)ptr);
    if ( f != NULL)
    {
        return f;
    }
    return (void *) NULL;
}

/* Get alias value from alias object*/
char *getAliasVal (const void *ptr)
{
    aliasListSt *pAliasListSt = (aliasListSt *)ptr;
    return pAliasListSt->val;
}

/* @Function Name: insertAliasInList
 * @brief: Insert/modify alias in list or if exist return the value
 * @return: True on Success, Otherwise False
 */
BOOL insertAliasInList (void *ptr)
{
    char *token1 = NULL;
    char *token2 = NULL;
    char *key = NULL;
    char *val = NULL;
    BOOL ret = TRUE;

    char *str = (char *) ptr;

    if(str == NULL)
        return FALSE;

    token1 = strtok (str, "=");
    token2 = strtok (NULL, "=");

    if ((token1 != NULL) &&  (token2 != NULL))
    {
        key = (char *) malloc (strlen(token1)+ 1);
        val = (char *) malloc (strlen(token2)+ 3);

        if ((key != NULL) && (val != NULL))
        {
            aliasListSt *pAliasListSt = NULL;
            strncpy(key, token1, strlen(token1));
            // token is not NULL terminated
            key[strlen(token1)] = '\0';

            if ((token2[0] == '"') && (token2[strlen(token2)-1] == '"'))
            {
                strncpy(val, token2, strlen(token2));
                val[strlen(token2)] = '\0';
            }
            else
            {
                strncpy(&val[0], token2, strlen(token2));
                val[strlen(token2)] = '\0';
            }

            pAliasListSt = getAliasObjByKey(key);

            // if Key is new and
            if ( pAliasListSt == NULL)
            {

                LOGD ("%s:%d : KEY = [%s] : VAL = [%s] \n", __func__, __LINE__, key, val);
                addAliasEntry (key, val);
            }
            else
            {
                // validate if having same alias value, if same then no need to handle and free the current key and val
                if (strcmp ((char *)pAliasListSt->val, (char *)val) == 0)
                {
                    free (key);
                    free (val);
                }
                else
                {
                    // update alias value in alias list with new value.
                    free (pAliasListSt->val);
                    pAliasListSt->val = NULL;
                    pAliasListSt->val = val;
                }
            }

        }
        else
        {
            if ((key == NULL) && (val == NULL)) { LOGE("%s:%d - OOM \n", __func__, __LINE__); }
            else if (key) { free(key);}
            else { free(val); }
            LOGE("%s:%d - OOM \n", __func__, __LINE__);
            ret = FALSE;
        }
    }
    else
    {
        /* pull alias value if exist already */
        if (token1)
        {
            void *temp = NULL;
            temp = getAliasObjByKey ((const void *)token1);
            if (temp != NULL)
            {
                aliasListSt *pAliasListSt = NULL;
                pAliasListSt = (aliasListSt *)temp;
                LOGD ("alias %s=%s \n", pAliasListSt->key, pAliasListSt->val);
            }
            else
            {
                ret = FALSE;
                LOGI ("mybash : alias not exist : [%s]\n", token1);

            }
        }
        if (token2)
        {
            ret = FALSE;
            LOGI ("mybash : alias not exist : [%s]\n", token2);
        }

    }

    key = NULL;
    val = NULL;
    return ret;

}

/* @Function Name: closeAliasList
 * @brief: store list in .alias file and delete the list
 * @return: None
*/
void closeAliasList(void)
{
    if (storeAliasList() != TRUE)
    {
        LOGD ("failed to write alias in file \n");
    }
    deleteAliasList();
}

