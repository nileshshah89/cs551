/*
 * @file utils.h
 *
 * @brief utility header file
 *
 * @author Vijay  Gupta (A20330677)
 *         Nilesh Shah  ()
 *         Kiran        ()
 *
 */

#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#include <sys/wait.h>
#include <sys/types.h>

#include <signal.h>


#define DEBUG_ALL 0
#define LOG_INFO 0

#define LOGE(f_, ...) printf((f_), ##__VA_ARGS__)


#if (defined(DEBUG_ALL) && (DEBUG_ALL == 1))
#define LOGD(f_, ...) printf((f_), ##__VA_ARGS__)
#if (defined(LOG_INFO) && (LOG_INFO == 1))
#define LOGI(f_, ...) printf((f_), ##__VA_ARGS__)
#else
#define LOGI(f_, ...) /* nothing */
#endif
#else
#define LOGD(f_, ...) /* nothing */
#define LOGI(f_, ...) /* nothing */

#endif


/* Basic data type define */
typedef int BOOL;
typedef unsigned int UINT;


#define TRUE  1
#define FALSE 0

#endif // UTILS_H
