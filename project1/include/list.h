/*
 * @file list.h
 *
 * @brief Generic linked list structure declaration
 *
 * @author Vijay  Gupta (A20330677)
 *         Nilesh Shah  ()
 *         Kiran        ()
 *
 */

#ifndef LIST_H
#define LIST_H

#include "utils.h"

typedef struct list {
    void        *pVal ;
    struct list *pNext;
} List;

void insertList (List **, void *);
void showList (List *, void (*)(void *));
void *findVal (List *, BOOL (*)(void *, void *), void *);
void deleteVal (List **, BOOL (*)(void *, void *), void *);
void deleteList (List **, void (*)(void *));

#endif // LIST_H
