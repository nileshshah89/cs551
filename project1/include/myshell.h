/*
 * @file 
 *
 * @brief 
 *
 * @author Vijay  Gupta, Nilesh Shah, Kiran
 *
 */

#ifndef MYSHELL_H
#define MYSHELL_H

#include "utils.h"

void startShell();
void printCurrentDirector();
void getCommand();

#endif //MYSHELL_H
