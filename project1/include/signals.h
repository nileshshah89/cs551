/*
 * @file 
 *
 * @brief 
 *
 * @author Vijay  Gupta, Nilesh Shah, Kiran
 *
 */

#ifndef SIGNAL_H
#define SIGNAL_H

#include "utils.h"

// Prototype for signal handler
void handleSignal(int signalNum);
void installSignalHandlers();

#endif //SIGNAL_H
