/*
 * @file alias.h
 *
 * @brief alias feature function declaration.
 *
 * @author Vijay  Gupta, Nilesh Shah, Kiran
 *
 */

#ifndef ALIAS_H
#define ALIAS_H

#include "utils.h"
#include "list.h"

#define LINUX_DEBUG


typedef struct aliasList
{
    char *key;
    char *val;
} aliasListSt;

#if defined(LINUX_DEBUG)
#define ALIAS_FILE ".alias"
#else
#define ALIAS_FILE "<need-to-define-path>/.alias"
#endif


BOOL createAliasList (void);
BOOL addAlias();
// executeAlias() // to run the alias
//BOOL getAliasVal(void);
BOOL storeAliasList (void);
void deleteAliasList(void);
void closeAliasList(void);
void showAllAlias(void);
void *getAliasObjByKey(const void *);
char *getAliasVal (const void *);
BOOL insertAliasInList (void *);

#endif // ALIAS_H
