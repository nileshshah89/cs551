/*
 * @file alias.h
 *
 * @brief alias feature function declaration.
 *
 * @author Vijay  Gupta, Nilesh Shah, Kiran
 *
 */

#ifndef MAIN_H
#define MAIN_H

#define SIZE 1024
#define PROFILE_VARIABLE_SIZE 1024

#define MAX_COMMANDS 128

#endif //MAIN_H
