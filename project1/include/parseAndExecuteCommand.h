/*
 * @file parseAndExecuteCommand.h
 *
 * @brief 
 *
 * @author Vijay  Gupta, Nilesh Shah, Kiran
 *
 */

#ifndef COMMANDLINEPARSER_H
#define COMMANDLINEPARSER_H

#include "utils.h"

typedef struct _commandNode {
    char *command;
    struct _commandNode *next;
}commandNode;


void parseAndExecuteCommand(char *);
void executeCommand(char *, int waitForChildCompletion);
void createChild(char **, int waitForChildCompletion);
void changeDirectory(char **);

#endif //COMMANDLINEPARSER_H
