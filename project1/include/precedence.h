/*
 * @file precedence.h
 *
 * @brief Precedence and validation operations
 *
 * @author Kiran, Vijay, Nilesh
 *
 */

#ifndef PRECEDENCE_H
#define PRECEDENCE_H

#include<stdio.h>
#include<stdlib.h>
#include "utils.h"

/* structure of a stack node */
struct parenthesesNode
{
   char data;
   struct parenthesesNode *next;
};
 
/* push an item to stack*/
void push(struct parenthesesNode** top_ref, int new_data);
 
/* pop an item from stack*/
int pop(struct parenthesesNode** top_ref);

/* Returns 1 if character1 and character2 are matching left
   and right Parentheses */
BOOL isMatchingPair(char character1, char character2);

/*Return 1 if expression has balanced Parentheses */
BOOL areParenthesesBalanced(char exp[], int* depth);

#endif // PRECEDENCE_H
